/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aaattwoo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/29 12:28:04 by aaattwoo          #+#    #+#             */
/*   Updated: 2021/12/01 21:39:21 by aaattwoo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_char(char *h, char *ha)
{
	(*h)++;
	*ha = *h + 1;
}

void	ft_print_comb(void)
{
	char	abc[3];

	abc[0] = '0';
	abc[1] = '1';
	abc[2] = '2';
	while (abc[0] <= '7')
	{
		while (abc[1] <= '8')
		{	
			while (abc[2] <= '9')
			{
				write (1, abc, 3);
				if (abc[0] != '7' || abc[1] != '8' || abc[2] != '9')
					write(1, ", ", 2);
				(abc[2])++;
			}
			ft_char(&abc[1], &abc[2]);
		}
		ft_char(&abc[0], &abc[1]);
		abc[1]--;
	}
}

int	main(void)
{
	ft_print_comb();
	return (0);
}
