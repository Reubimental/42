/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_reverse_alphabet.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aaattwoo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/28 23:33:16 by aaattwoo          #+#    #+#             */
/*   Updated: 2021/11/30 17:00:22 by aaattwoo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_print_reverse_alphabet(void)
{
	int	t;

	t = 122;
	while (t >= 97)
	{
		write(1, &t, 1);
		t--;
	}
}

/*
 * int	main(void)
{
	ft_print_reverse_alphabet();
}
*/
