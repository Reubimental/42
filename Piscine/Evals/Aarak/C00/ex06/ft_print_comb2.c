/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aaattwoo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/29 12:46:16 by aaattwoo          #+#    #+#             */
/*   Updated: 2021/12/01 23:49:25 by aaattwoo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_char(char *h, char *ha)
{
	(*h)++;
	*ha = *h + 1;
}

void	ft_print_comb2(void)
{
	char	ab[2];
	char	aa[2];

	ab[0] = '0';
	ab[1] = '1';
	aa[0] = '0';
	aa[1] = '0';
	while (ab[0] <= '8' && aa[0] <= '7')
	{
		while (ab[1] <= '9' && aa[1] <= '8')
		{
			write(1, aa, 2);
			write(1, " ", 1);
			write (1, ab, 2);
			if (ab[0] != '8' || ab[1] != '9' && aa[0] != '7' || aa[1] != '8')
				write(1, ", ", 2);
			(ab[1])++;
			(aa[1])++;
		}
		ft_char(&ab[0], &ab[1]);
		ft_char(&aa[0], &aa[1]);
	}
	ab[1]--;
	aa[1]--;
}

int	main(void)
{
	ft_print_comb2();
	return (0);
}
