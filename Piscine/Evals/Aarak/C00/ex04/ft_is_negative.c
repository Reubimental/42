/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_negative.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aaattwoo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/26 03:14:12 by aaattwoo          #+#    #+#             */
/*   Updated: 2021/11/29 00:31:14 by aaattwoo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_is_negative( int n)
{
	if (n < 0)
		write(1, "N", 1);
	else
		write(1, "P", 1);
	n++;
}

/* int	main(void)
{
	ft_is_negative(-777);
	return (0);
}*/
