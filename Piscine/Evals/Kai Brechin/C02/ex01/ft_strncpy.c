/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kbrechin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/06 15:21:17 by kbrechin          #+#    #+#             */
/*   Updated: 2021/12/09 21:33:10 by rkabzins         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
char	*ft_strncpy(char *dest, char *src, unsigned int n)
{
	unsigned int	p;

	p = 0;
	while (p < n && src[p] != '\0')
	{
		dest[p] = src[p];
		p++;
	}
	while (p < n)
	{
		dest[p] = '\0';
		p++;
	}
	return (dest);
}

/*
#include <stdio.h>
#include <string.h>
int	main()
{
	char *returned1;
	char *returned2;

	char destination[100] = "012345678901234567890";
	char source[100] = "My";
	returned1 =ft_strncpy(destination, source, 5);
	printf("%s\n", destination);
	printf("%s\n", source);
	printf("%s\n\n\n", returned1);
	char destination2[100] = "012345678901234567890";
	char source2[100] = "My";
	returned2 = strncpy(destination2, source2, 5);
	printf("%s\n", destination2);
	printf("%s\n", source2);
	printf("%s\n", returned2);
}
*/
