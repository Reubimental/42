/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_numeric.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kbrechin <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/07 17:56:40 by kbrechin          #+#    #+#             */
/*   Updated: 2021/12/09 09:57:02 by kbrechin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <stdio.h>

int	ft_str_is_numeric(char *str)
{
	int	z;

	z = 0;
	while (str[z])
	{
		if (str[z] < '0' || str[z] > '9')
		{
			return (0);
		}
		z++;
	}
	return (1);
}
/*
int main(void)
{
	int p =	ft_str_is_numeric("111");
	printf("%d\n", p);
}
*/
