/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarmstro <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/09 11:01:12 by jarmstro          #+#    #+#             */
/*   Updated: 2021/12/09 17:07:18 by jarmstro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include<stdio.h>

int	ft_iterative_power(int nb, int power);

int	main(void)

{
	printf("%d\n", ft_iterative_power(2, -2));
	printf("%d\n", ft_iterative_power(-2, 0));
	printf("%d\n", ft_iterative_power(-2 , 7));
	printf("%d\n", ft_iterative_power(2, 7));
	printf("%d\n", ft_iterative_power(-3, 2));
	return (0);
}
