/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarmstro <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/09 11:01:12 by jarmstro          #+#    #+#             */
/*   Updated: 2021/12/09 13:07:08 by jarmstro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include<stdio.h>

int	ft_sqrt(int index);

int	main(void)

{
	//printf("%d\n", ft_fibonacci(-1));
	printf("%d\n", ft_sqrt(-4));
	printf("%d\n", ft_sqrt(-1));
	printf("%d\n", ft_sqrt(0));
	printf("%d\n", ft_sqrt(1));
	printf("%d\n", ft_sqrt(2));
	printf("%d\n", ft_sqrt(4));
	printf("%d\n", ft_sqrt(6));
	printf("%d\n", ft_sqrt(7));
	printf("%d\n", ft_sqrt(14641));
	return (0);
}
