/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mpage <mpage@student.42adel.org.au>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/29 21:59:46 by mpage             #+#    #+#             */
/*   Updated: 2021/11/29 21:59:54 by mpage            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_print_2_2_digit_integers(int integer1, int integer2)
{
	char	character1;
	char	character2;

	character1 = '0' + (integer1 / 10);
	character2 = '0' + (integer1 % 10);
	write(1, &character1, 1);
	write(1, &character2, 1);
	write(1, " ", 1);
	character1 = '0' + (integer2 / 10);
	character2 = '0' + (integer2 % 10);
	write(1, &character1, 1);
	write(1, &character2, 1);
}

void	ft_print_comb2(void)
{
	int	integer1;
	int	integer2;

	integer1 = 00;
	while (integer1 <= 98)
	{
		integer2 = integer1 + 1;
		while (integer2 <= 99)
		{
			ft_print_2_2_digit_integers(integer1, integer2);
			if (!(integer1 == 98 && integer2 == 99))
			{
				write(1, ", ", 2);
			}
			integer2++;
		}
		integer1++;
	}
}
