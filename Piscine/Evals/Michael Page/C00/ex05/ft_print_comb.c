/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mpage <mpage@student.42adel.org.au>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/28 19:18:12 by mpage             #+#    #+#             */
/*   Updated: 2021/11/28 21:11:15 by mpage            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_print_3_characters(char character1, char character2, char character3)
{
	write(1, &character1, 1);
	write(1, &character2, 1);
	write(1, &character3, 1);
}

void	ft_print_comb(void)
{
	char	digit1;
	char	digit2;
	char	digit3;

	digit1 = '0';
	while (digit1 <= '7')
	{
		digit2 = digit1 + 1;
		while (digit2 <= '8')
		{
			digit3 = digit2 + 1;
			while (digit3 <= '9')
			{
				ft_print_3_characters(digit1, digit2, digit3);
				if (digit1 != '7')
				{
					write(1, ", ", 2);
				}
				digit3++;
			}
			digit2++;
		}
		digit1++;
	}
}
