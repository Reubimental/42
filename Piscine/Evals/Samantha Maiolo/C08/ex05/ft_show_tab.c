/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_show_tab.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: smaiolo <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/16 10:32:25 by smaiolo           #+#    #+#             */
/*   Updated: 2021/12/16 20:54:41 by rkabzins         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "ft_stock_str.h"

void	ft_printstr(char *str)
{
	while (*str)
	{
		write(1, str, 1);
		str++;
	}
}

void	ft_print(char x)
{
	write(1, &x, 1);
}

void	ft_putnbr(int nb)
{
	nb = (long int)nb;
	if (nb == INT_MIN)
	{
		write(1, "-2147483648", 11);
	}
	else if (nb > 9)
	{
		ft_putnbr(nb / 10);
		ft_putnbr(nb % 10);
	}
	else if (nb < 0)
	{
		nb = nb * -1;
		write(1, "-", 1);
		ft_putnbr(nb);
	}
	else
	{
		ft_print(nb + 48);
	}
}

void	ft_show_tab(struct s_stock_str *par)
{
	int	x;

	x = 0;
	while (par[x].str != 0)
	{
		ft_printstr(par[x].str);
		write(1, "\n", 1);
		ft_printnbr(par[x].size);
		write(1, "\n", 1);
		ft_printstr(par[x].copy);
		write(1, "\n", 1);
		x++;
	}
}
