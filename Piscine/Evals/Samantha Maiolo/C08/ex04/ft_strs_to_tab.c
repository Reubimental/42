/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strs_to_tab.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: smaiolo <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/16 10:04:38 by smaiolo           #+#    #+#             */
/*   Updated: 2021/12/16 20:53:55 by rkabzins         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_stock_str.h"

int	ft_strl(char *str)
{
	int	i;

	while (str[i] != '\0')
		i++;
	return (i);
}

char	*ft_strdup(char *src)
{
	char	*str;
	int		x;

	x = 0;
	while (src[x] != '\0')
	{
		x++;
	}
	str = malloc(x * sizeof(char) + 1);
	if (str == NULL)
		return (0);
	x = 0;
	while (src[x] != '\0')
	{
		str[x] = src[x];
		x++;
	}
	str[x] = '\0';
	return (str);
}

struc s_stock_str	*ft_strs_to_tab(int ac, char **av)
{
	struc	*ar;
	struc	*t;
	int		n;

	if (ac == 0)
		return (0);
	ar = malloc((ac + 1) * sizeof(struc s_stock_str));
	if (ar == NULL)
	{
		*ar = 0;
		return (0);
	}
	while (n < ac)
	{
		ar[n].size = ft_strl(av[n]);
		ar[n].str = av[n];
		ar[n].copy = ft_stirdup(av[n]);
	}
	ar[n] = (struc s_stock_str){0, 0, 0};
	return (ar);
}
