/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agraetz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/25 11:00:04 by agraetz           #+#    #+#             */
/*   Updated: 2021/11/29 16:19:25 by rkabzins         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <unistd.h>
#include <stdio.h>

void	ft_print_comb(void)
{	
	int	a;
	int	b;
	int	c;

	a = 48 - 1;
	while (++a <= 55)
	{
		b = a;
		while (++b <= 56)
		{
			c = b;
			while (++c <= 57)
			{
				write(1, &a, 1);
				write(1, &b, 1);
				write(1, &c, 1);
				if (a != 55)
				{
					write(1, ", ", 1);
				}
			}
		}
	}
}

int main(void)
{
	ft_print_comb();
}
