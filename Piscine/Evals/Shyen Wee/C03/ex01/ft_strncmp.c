/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lwee <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/08 19:04:44 by lwee              #+#    #+#             */
/*   Updated: 2021/12/13 21:05:19 by lwee             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_strncmp(char *s1, char *s2, unsigned int n)
{
	while (n > 0)
	{
		if (*s1 != *s2)
			return (*s1 - *s2);
		if (*s1 == 0)
			return (0);
		s1++;
		s2++;
		n--;
	}
	return (0);
}

/*
#include <stdio.h>

int	main(void)
{
	char	s1[]="hello";
	char	s2[]="hellEo";	

	printf("%u\n", ft_strncmp(s1, s2, 0));
	printf("%u\n", ft_strncmp(s1, s2, 5));
	printf("%u\n", ft_strncmp(s1, s2, 6));
	printf("%u\n", ft_strncmp(s1, s2, 7));
}
*/
