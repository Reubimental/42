/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lwee <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/08 21:13:10 by lwee              #+#    #+#             */
/*   Updated: 2021/12/13 21:08:23 by lwee             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strncat(char *dest, char *src, unsigned int nb)
{
	unsigned int	d;
	unsigned int	s;

	d = 0;
	while (dest[d])
		d++;
	s = 0;
	while (src[s] && nb > 0)
	{
		dest[d + s] = src[s];
		s++;
		nb--;
	}
	dest[d + s] = '\0';
	return (dest);
}

/*
#include <stdio.h>

int	main(void)
{
	char	dest[]="hello";
	char	src[]="world";


	printf("%s\n", ft_strncat(dest, src, 3));
}
*/
