/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lwee <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/08 22:09:37 by lwee              #+#    #+#             */
/*   Updated: 2021/12/10 04:57:09 by lwee             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
unsigned int	ft_strlen(char *str)
{
	unsigned int	n;

	n = 0;
	while (*str)
	{
		n++;
		str++;
	}
	return (n);
}

unsigned int	ft_strlcat(char *dest, char *src, unsigned int size)
{
	unsigned int	d;
	unsigned int	s;
	unsigned int	og_d;

	if (size <= ft_strlen(dest))
		return (size + ft_strlen(src));
	d = ft_strlen(dest);
	og_d = d;
	s = 0;
	while (src[s] && size - d - 1 > 0)
	{
		dest[d] = src[s];
		s++;
		d++;
	}
	dest[d] = 0;
	return (og_d + ft_strlen(src));
}

/*
#include <stdio.h>
#include <string.h>

int	main(void)
{
	char	dest[20] = "hello";
	char	src[20]="world";
	unsigned int	n;
	unsigned int	og_d;

	//n = strlcat(dest, src, 8);
	og_d = ft_strlen(dest);
	n = ft_strlcat(dest, src, 8);
	printf("Length source: %u\n", ft_strlen(src));
	printf("Length destination: %u\n", og_d);
	printf("Return size: %u\n", n);
	printf("Destination: %s\n", dest);
}
*/
